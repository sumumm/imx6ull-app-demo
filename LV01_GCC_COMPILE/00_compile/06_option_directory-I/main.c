#include <stdio.h>
#include "func1.h"
#include "func2.h"

int main(int argc, const char *argv[])
{
	printf("This is main file!\n");
	func1();
	func2();
	return 0;
}