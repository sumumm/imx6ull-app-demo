#include <stdio.h>

int main(int argc, const char *argv[])
{
	printf("This is main file!\n");
	#ifdef TEST
	printf("TEST=%d\n", TEST);
	#else
	printf("TEST is not defined!\n");
	#endif
	return 0;
}