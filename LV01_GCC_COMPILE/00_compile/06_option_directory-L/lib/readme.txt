cd src/                  # 进入func1.c 和 func2.c 源文件所在目录

gcc -c func1.c -o func1.o   # 编译func1.c，生成 func1.o
gcc -c func2.c -o func2.o   # 编译func2.c，生成 func2.o

ar -rsv libfunc.a func1.o func2.o  # 打包成静态库
mv libfunc.a ../lib/               # 拷贝到库文件目录下