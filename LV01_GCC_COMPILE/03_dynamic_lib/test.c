#if 0
#include <stdio.h>
#include "file.h"
int main(int argc, const char *argv[])
{
	int a = 2;
	int b = 3;
	int sum = 0;
	
	sum = mySum(a, b);
	printf("sum = %d\n",sum);
	printf("This is test.c test:global=%d\n", global);
	global = 30;
	myTest();
	
    return 0;
}
#else
#include <stdio.h>
#include <dlfcn.h>

int main(int argc, const char *argv[])
{
	int a = 2;
	int b = 3;
	int sum = 0;
    char *file_so_path="/home/sumu/1sharedfiles/linux_develop/imx6ull-app-demo/LV01_GCC_COMPILE/02_dynamic_lib/libfile.so";
	void *handler = dlopen(file_so_path, RTLD_LAZY);
    int (*pFunc)(int, int) = dlsym(handler, "mySum");
    int *pVar = dlsym(handler, "global");

	sum = pFunc(a, b);
	printf("sum = %d\n",sum);
	printf("This is test.c test:global=%d\n", *pVar);
	*pVar = 30;

    void (*mytest)(void) = dlsym(handler, "myTest");
	mytest();
	dlclose(handler);
    return 0;
}
#endif
