#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <string.h>
#include <errno.h>
#include <sys/mman.h>
#include <linux/fb.h>
#include <math.h>       //数学库函数头文件

#include <wchar.h>
#include "../../lib/freetype-2.8/include/freetype2/ft2build.h"
#include "../../lib/freetype-2.8/include/freetype2/freetype/freetype.h"

#define FB_DEV      "/dev/fb0"      //LCD设备节点

#define argb8888_to_rgb565(color)   ({ \
            unsigned int temp = (color); \
            ((temp & 0xF80000UL) >> 8) | \
            ((temp & 0xFC00UL) >> 5) | \
            ((temp & 0xF8UL) >> 3); \
            })

typedef struct __LCD_DEV_COLOR{
    struct fb_bitfield red;
    struct fb_bitfield green;
    struct fb_bitfield blue;
} COLOR_TYPE;
typedef struct __LCD_DEV_CTL{
    uint8_t dir;         // LCD显示方向，0-横向显示（PCB板丝印向右），1-逆时针旋转90度(PCB板丝印向下)
    
    // lcd硬件信息
    uint16_t lcd_width;
    uint16_t lcd_height;
    uint32_t lcd_line_length;
    uint16_t lcd_bit_per_pixel;
    //COLOR_TYPE lcd_color;

    uint16_t max_width;
    uint16_t max_height;

    uint16_t *fb_base;   // 显存起始地址，unsigned short 类型指针, RGB565
    uint32_t fb_size;    // 显存大小
}LCD_DEV_CTRL;

static LCD_DEV_CTRL lcd_dev = {0};

static int fd = -1;

static FT_Library library;
static FT_Face face;

uint8_t freeTypeDebug = 0;
#define FREE_TYPE_DBG(FMT, ARGS...) \
        do \
        { \
            if(freeTypeDebug) \
            { \
                printf(FMT, ##ARGS); \
            } \
        } while(0)

static void lcd_dir_set(uint8_t dir)
{
    LCD_DEV_CTRL *p_lcd_dev = &lcd_dev;
    p_lcd_dev->dir = dir;
    switch(p_lcd_dev->dir)
    {
        case 0:
            p_lcd_dev->max_width = p_lcd_dev->lcd_width;
            p_lcd_dev->max_height = p_lcd_dev->lcd_height;
            break;
        case 1:
            p_lcd_dev->max_width = p_lcd_dev->lcd_height;
            p_lcd_dev->max_height = p_lcd_dev->lcd_width;
            break;
        default:
            p_lcd_dev->max_width = p_lcd_dev->lcd_width;
            p_lcd_dev->max_height = p_lcd_dev->lcd_height;
            break;
    }
}
static int fb_dev_init(void)
{
    struct fb_var_screeninfo fb_var = {0};
    struct fb_fix_screeninfo fb_fix = {{0}};

    /* 打开framebuffer设备 */
    fd = open(FB_DEV, O_RDWR);
    if (0 > fd) 
    {
        fprintf(stderr, "open error: %s: %s\n", FB_DEV, strerror(errno));
        return -1;
    }

    /* 获取framebuffer设备信息 */
    ioctl(fd, FBIOGET_VSCREENINFO, &fb_var);
    ioctl(fd, FBIOGET_FSCREENINFO, &fb_fix);

    lcd_dev.fb_size = fb_fix.line_length * fb_var.yres;

    lcd_dev.lcd_width = fb_var.xres;
    lcd_dev.lcd_height = fb_var.yres;
    lcd_dev.lcd_line_length = fb_fix.line_length;
    lcd_dev.lcd_bit_per_pixel = fb_var.bits_per_pixel;

    /* 内存映射 */
    lcd_dev.fb_base = (uint16_t *)mmap(NULL, lcd_dev.fb_size, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
    if (MAP_FAILED == (void *)lcd_dev.fb_base) 
    {
        perror("mmap error");
        close(fd);
        return -1;
    }
    lcd_dir_set(1);
    printf("分辨率: %d*%d fb_base=0x%x, fb_size=%d, dir=%d lcd_max_width=%d lcd_max_height=%d\n",
            lcd_dev.lcd_width, lcd_dev.lcd_height, (uint32_t)lcd_dev.fb_base, lcd_dev.fb_size,
            lcd_dev.dir, lcd_dev.max_width, lcd_dev.max_height);
    /* LCD背景刷成黑色 */
    memset(lcd_dev.fb_base, 0xFF, lcd_dev.fb_size);
    return 0;
}

static int freetype_init(const char *font, int angle, int fontSize)
{
    FT_Error error;
    FT_Vector pen;
    FT_Matrix matrix;
    float rad;      //旋转角度

    /* FreeType初始化 */
    FT_Init_FreeType(&library);

    /* 加载face对象 */
    error = FT_New_Face(library, font, 0, &face);
    if (error) 
    {
        fprintf(stderr, "FT_New_Face error: %d\n", error);
        exit(EXIT_FAILURE);
    }

    /* 原点坐标 */
    pen.x = 0 * 64;
    pen.y = 0 * 64;     //原点设置为(0, 0)

    /* 2x2矩阵初始化 */
    rad = (1.0 * angle / 180) * M_PI;   //（角度转换为弧度）M_PI是圆周率
#if 0       //非水平方向
    matrix.xx = (FT_Fixed)( cos(rad) * 0x10000L);
    matrix.xy = (FT_Fixed)(-sin(rad) * 0x10000L);
    matrix.yx = (FT_Fixed)( sin(rad) * 0x10000L);
    matrix.yy = (FT_Fixed)( cos(rad) * 0x10000L);
#endif

#if 1       //斜体  水平方向显示的
    matrix.xx = (FT_Fixed)( cos(rad) * 0x10000L);
    matrix.xy = (FT_Fixed)( sin(rad) * 0x10000L);
    matrix.yx = (FT_Fixed)( 0 * 0x10000L);
    matrix.yy = (FT_Fixed)( 1 * 0x10000L);
#endif

    /* 设置 */
    FT_Set_Transform(face, &matrix, &pen);
    FT_Set_Pixel_Sizes(face, fontSize, 0);    //设置字体大小

    return 0;
}

// 注意，这里并未考虑超出边界的时候，可能会有问题，当时是学习竖屏转换，并未对此做优化
static void lcd_draw_character(int x, int y, const wchar_t *str, unsigned int color)
{
    unsigned short rgb565_color = argb8888_to_rgb565(color);//得到RGB565颜色值
    FT_GlyphSlot slot = face->glyph;
    size_t len = wcslen(str);   //计算字符的个数
    long int temp;
    int n;
    int i, j, p, q, row_cnt;
    int max_x, max_y, start_y, start_x;

    // 循环加载各个字符
    for (n = 0; n < len; n++) 
    {
        // 加载字形、转换得到位图数据
        if (FT_Load_Char(face, str[n], FT_LOAD_RENDER))
            continue;
        FREE_TYPE_DBG("[%d,%d][slot]bitmap_left=%d bitmap_top=%d, bitmap:rows=%d width=%d\n",
                    x, y,
                    slot->bitmap_left, slot->bitmap_top,
                    slot->bitmap.rows,slot->bitmap.width);
        // 计算字形轮廓上边y坐标起点位置 注意是减去bitmap_top
        // 这里相当于这个y是下边界了
        start_y = y - slot->bitmap_top; 
        if (0 > start_y) 
        {
            //如果为负数 如何处理？？
            q = -start_y;
            j = 0;
        }
        else 
        {      
            // 正数又该如何处理??
            q = 0;
            j = start_y;
        }

        max_y = start_y + slot->bitmap.rows;// 计算字形轮廓下边y坐标结束位置
        if (max_y > (int)lcd_dev.max_height)
        {
            max_y = lcd_dev.max_height;
        }

        for (row_cnt=0; j < max_y; j++, q++, row_cnt++) // y表示的是第几行，这里是 start_y - max_y 行逐行扫描
        {
            start_x = x + slot->bitmap_left;  // 起点位置要加上左边空余部分长度
            if (0 > start_x) 
            {
                p = -start_x;
                i = 0;
            }
            else 
            {
                p = 0;
                i = start_x;
            }

            max_x = start_x + slot->bitmap.width;
            if (max_x > (int)lcd_dev.max_width)
            {
                max_x = lcd_dev.max_width;
            }
            FREE_TYPE_DBG("j=%d row %2d ", j, row_cnt);
            for (; i < max_x; i++, p++) 
            {
                // 如果数据不为0，则表示需要填充颜色
                
                if (slot->bitmap.buffer[q * slot->bitmap.width + p])
                {
                    if(lcd_dev.dir == 1) // 竖屏显示
                    {
                        temp = (lcd_dev.lcd_height - 1 - i)*lcd_dev.lcd_width + j;
                    }
                    else  // 横屏显示
                    {
                        temp = j*lcd_dev.lcd_width + i;
                    }
                    lcd_dev.fb_base[temp] = rgb565_color;
                    FREE_TYPE_DBG("*");
                }
                else 
                {
                   FREE_TYPE_DBG(" ");
                }
            }
            FREE_TYPE_DBG("\n");
        }
        FREE_TYPE_DBG("\n");
        //调整到下一个字形的原点
        x += slot->advance.x / 64;  //26.6固定浮点格式
        y -= slot->advance.y / 64;
    }
}

int main(int argc, char *argv[])
{
    if (argc < 5)
    {
        printf("Usage : %s <font_file> <angle> <fontSize> <dir>\n", argv[0]);
        printf("\t Example: ./app_demo ./data/simsun.ttc 10 24 0\n");
        return -1;
    }
    /* LCD初始化 */
    if (fb_dev_init())
    {
        exit(EXIT_FAILURE);
    }

    /* freetype初始化 */
    if (freetype_init(argv[1], atoi(argv[2]),  atoi(argv[3])))
    {
        exit(EXIT_FAILURE);
    }
    lcd_dir_set(atoi(argv[4]));
    /* 在LCD上显示中文 */
    int y = lcd_dev.lcd_height * 0.25;
    lcd_draw_character(50, 100, L"路漫漫其修远兮，吾将上下而求索", 0x000000);
    lcd_draw_character(50, y+100, L"莫愁前路无知己，天下谁人不识君", 0x9900FF);
    
    /* 退出程序 */
    FT_Done_Face(face);
    FT_Done_FreeType(library);
    munmap(lcd_dev.fb_base, lcd_dev.fb_size);
    close(fd);
    exit(EXIT_SUCCESS);
}
