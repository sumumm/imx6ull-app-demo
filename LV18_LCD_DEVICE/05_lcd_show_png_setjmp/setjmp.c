#include <stdio.h>
#include <stdlib.h>
#include <setjmp.h>

static jmp_buf buf;
static int cnt = 0;

static void hello(void)
{
    cnt++;
    printf("hello world!\n");
    longjmp(buf,cnt); // 执行跳转
    printf("Nice to meet you!\n");
}

int main(void)
{
    int ret = -1;

    ret = setjmp(buf); //设置跳转
    printf("setjmp ret = %d\n", ret);
    if(ret == 0) 
    {
        printf("First return\n");
    }
    else
    {
        printf("Second return\n");
    }
    if(cnt < 2)
    {
        hello();
    }
    printf("exit main\n");
    exit(0);
}
