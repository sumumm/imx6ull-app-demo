多进程服务器

一个遗留问题：
server中要用到pthread库，但是Makefile这样写有问题，要吧pthread放在最后面才行：
```Makefile
$(TARGET): $(OBJS)
#	$(CC) $(CFLAGS) $(INCLUDE) $(STATIC_LIB_FLAG) $(STATIC_LIB) $(OBJS) -o $@ # 不知道为什么这么写不行，没有深究
	$(CC) $(CFLAGS) $(INCLUDE) $(STATIC_LIB_FLAG) $(OBJS) -o $@ $(STATIC_LIB)

$(COBJS) : $(OBJ_DIR)%.o : %.c
#	$(CC) $(CFLAGS) $(STATIC_LIB) -c  $(INCLUDE) $< -o $@ 
	$(CC) $(CFLAGS) -c  $(INCLUDE) $< -o $@ 
```