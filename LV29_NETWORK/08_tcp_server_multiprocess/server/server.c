#include <stdio.h>      /* perror */
#include <stdlib.h>     /* exit atoi */
#include <errno.h>      /* errno号 */
#include <sys/types.h>  /* socket           bind listen accept send fork */
#include <sys/socket.h> /* socket inet_addr bind listen accept send */
#include <strings.h>    /* bzero */
#include <arpa/inet.h>  /* htons  inet_addr inet_pton */
#include <netinet/in.h> /* ntohs  inet_addr inet_ntop*/
#include <unistd.h>     /* close fork */
#include <string.h>     /* strlen strcat*/
#include <signal.h>    /* signal */
#include <sys/wait.h>  /* waitpid */

void usage(char *str); /* 提示信息打印函数 */
void clientDataHandle(void *arg);/* 进程处理函数 */
void sigChildHandle(int signo);  /* 信号回收子进程 */
int main(int argc, char *argv[])
{
	/* 参数判断及端口号处理 */
	int port = -1;
	if (argc != 3)/* 参数数量不对时打印提示信息 */
	{
		usage(argv[0]);
		exit(-1);
	}
	port = atoi(argv[2]);/* 字符串转数字 */
	if (port < 5000)
	{
		usage(argv[0]);
		exit(-1);
	}
	/* 注册信号处理函数，实现进程结束自动回收 */
	signal(SIGCHLD, sigChildHandle);
	/* 1.创建套接字，得到socket描述符 */
	int socket_fd = -1; /* 接收服务器端socket描述符 */
	if((socket_fd = socket(AF_INET, SOCK_STREAM, 0)) < 0)/* SOCK_STREAM,使用TCP协议 */
	{
		perror ("socket");
		exit(-1);
	}
	/* 允许绑定地址快速重用 */
	int b_reuse = 1;
	setsockopt(socket_fd, SOL_SOCKET, SO_REUSEADDR, &b_reuse, sizeof (int));
	/* 2.将套接字与指定端口号和IP进行绑定 */
	/* 2.1填充struct sockaddr_in结构体变量 */
	struct sockaddr_in sin;
	bzero (&sin, sizeof (sin));  /* 将内存块（字符串）的前n个字节清零 */
	sin.sin_family = AF_INET;    /* 协议族, IPv4 */
	sin.sin_port = htons(port);  /* 网络字节序的端口号 */
	if(inet_pton(AF_INET, argv[1], (void *)&sin.sin_addr) != 1)/* 填充IP地址,INADDR_ANY表示允许监听任意IP,但是它其实是(in_addr_t) 0x00000000 */
	{
		perror ("inet_pton");
		exit(-1);
	}
	/* 2.2绑定 */
	if(bind(socket_fd, (struct sockaddr *)&sin, sizeof(sin)) < 0)
	{
		perror("bind");
		exit(-1);
	}
	/*3.调用listen()把主动套接字变成被动套接字 */
	if (listen(socket_fd, 5) < 0)
	{
		perror("listen");
		exit(-1);
	}
	printf ("Server starting....OK!\n");
	/*4.阻塞等待客户端连接请求所需变量定义 */
	int newfd = -1;
	struct sockaddr_in cin;          /* 用于存储成功连接的客户端的IP信息 */
	socklen_t addrlen = sizeof(cin);	
	/* 5.打印成功连接的客户端的信息相关变量定义 */
	char ipv4_addr[16];
	/* 6.多进程程相关变量定义 */
	pid_t pid = -1;
	while(1)
	{
		/* 等待连接 */
		if ((newfd = accept(socket_fd, (struct sockaddr *)&cin, &addrlen)) < 0)
		{
			perror("accept");
			exit(-1);
		}
		/* 创建进程处理客户端请求 */
		pid = fork();
		if(pid < 0)/* 进程创建失败 */
		{
			perror("fork");
			break;
		}
		else if(pid > 0)/* 父进程 */
		{
			printf("%d\n", newfd);
			close(newfd);/* 子进程会复制父进程的资源包括文件描述符，所以父进程这里不再需要这个新的socket描述符 */
		}
		else /* 子进程，这里会复制之前成功连接到服务器的客户端而产生的的新的newfd */
		{
			close(socket_fd);/* 子进程用于处理客户端请求，所以不需要用于监听的那个socket套接字了 */
			/* 获取连接成功的客户端信息 */
			if (!inet_ntop(AF_INET, (void *)&cin.sin_addr, ipv4_addr, sizeof(cin)))
			{
				perror ("inet_ntop");
				exit(-1);
			}
			printf ("Clinet(%s:%d) is connected successfully![newfd=%d]\n", ipv4_addr, ntohs(cin.sin_port), newfd);
			clientDataHandle(&newfd);/* 处理数据 */
			exit(0);	
		}
	}
	/* 7.关闭文件描述符 */
	close(socket_fd);

	return 0;
}

/**
 * @Function: usage
 * @Description: 用户提示信息打印函数
 * @param str : 当前应用程序命令字符串，一般是./app
 * @return  : none
 */
void usage(char *str)
{
	printf ("\n%s serv_ip serv_port", str);
	printf ("\n\t serv_ip: server ip address");
	printf ("\n\t serv_port: server port(>5000)\n\n");
	printf ("\n\t Attention: The IP address must be the IP address of the local nic or 0.0.0.0 \n\n");
}

/**
 * @Function: clientDataHandle
 * @Description: 子进程处理数据函数
 * @param arg: 得到客户端连接产生新的套接字文件描述符
 * @return  : none
 */
void clientDataHandle(void *arg)
{
	int newfd = *(int *) arg;
	int ret = -1;
	char buf[BUFSIZ];
	char replay[BUFSIZ];
	printf ("handler process: newfd =%d\n", newfd);
	/* 数据读写 */
	while (1)
	{
		bzero(buf, BUFSIZ);
		bzero(replay, BUFSIZ);
		/* 读取客户端数据 */
		do
		{
			ret = read(newfd, buf, BUFSIZ-1);
		}while (ret < 0 && EINTR == errno);
		if(ret < 0)
		{
			perror ("read");
			exit(-1);
		}
		if(!ret) break;/* 客户端已经关闭 */
		printf ("Receive client[%d] data: %s\n", newfd, buf);
		/* 对客户端做出回应 */
		strcat(replay, buf);
		ret = send(newfd, replay, strlen(replay), 0);
		if(ret < 0)
		{
			perror("send");
			exit(-1);
		}
		/* 判断是否需要退出 */
		if(!strncasecmp(buf, "quit", strlen("quit")))  	//用户输入了quit字符
		{
			printf ("Client[%d] is exiting!\n", newfd);
			break;
		}
	}
	/* 6.关闭文件描述符 */
	close(newfd);
}

/**
 * @Function: sigChildHandle
 * @Description: 通过信号回收子进程
 * @param signo: 检测到的信号(子进程结束时会发出SIGCHLD信号)
 * @return  : none
 */
void sigChildHandle(int signo)
{
	if(signo == SIGCHLD)
	{
		waitpid(-1, NULL,  WNOHANG);
	}
}
