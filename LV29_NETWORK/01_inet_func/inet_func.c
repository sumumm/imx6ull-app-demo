#include <stdio.h>
#include <stdlib.h>
#include <arpa/inet.h>

#define IPV4_ADDR "192.168.1.100"

int main(int argc, const char *argv[])
{
    /* code */ 
    struct in_addr addr;
    inet_pton(AF_INET, IPV4_ADDR, &addr);
    printf("ip addr: 0x%x\n", addr.s_addr);
    exit(0);
}