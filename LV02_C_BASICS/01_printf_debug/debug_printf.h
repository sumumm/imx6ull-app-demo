#ifndef __DEBUG_PRINTF_H__
#define __DEBUG_PRINTF_H__

#include <stdio.h>

#define DEBUG_ENABLE  1           /* 是否开启调试信息 */
#define LOG_LEVEL     DEBUG_ALL   /* 调试信息显示级别 */
 
#define ERROR   "ERROR"           /* 错误 */
#define WARN    "WARN "           /* 警告 */
#define INFO    "INFO "           /* 信息 */
/* 颜色定义——字体颜色 */
#define CLS_ALL        "\033[0m"  /* 清除所有颜色 */
#define F_BLACK        "\033[30m" /* 黑色字体 */
#define F_RED          "\033[31m" /* 红色字体 */
#define F_GREEN        "\033[32m" /* 绿色字体 */
#define F_YELLOW       "\033[33m" /* 黄色字体 */
#define F_BLUE         "\033[34m" /* 蓝色字体 */
#define F_PURPLE       "\033[35m" /* 紫色字体 */
#define F_CYAN         "\033[36m" /* 青色字体 */
#define F_WHITE        "\033[37m" /* 白色字体 */
/* 颜色定义——背景颜色 */
#define B_BLACK        "\033[40m" /* 黑色背景 */
#define B_RED          "\033[41m" /* 红色背景 */
#define B_GREEN        "\033[42m" /* 绿色背景 */
#define B_YELLOW       "\033[43m" /* 黄色背景 */
#define B_BLUE         "\033[44m" /* 蓝色背景 */
#define B_PURPLE       "\033[45m" /* 紫色背景 */
#define B_CYAN         "\033[46m" /* 青色背景 */
#define B_WHITE        "\033[47m" /* 白色背景 */
/* 颜色定义——背景的字体 */
#define BLACK_RED      "\033[30;41m" /* 红底黑字 */
#define BLACK_GREEN    "\033[30;42m" /* 绿底黑字 */
#define BLACK_YELLOW   "\033[30;43m" /* 黄底黑字 */

/* 调试等级枚举类型定义 */
enum DEBUG_LEVEL
{
    DEBUG_OFF   = 0,
    DEBUG_ERROR = 1,
    DEBUG_WARN  = 2,
    DEBUG_INFO  = 3,
    DEBUG_ALL   = 4
};
//===========================================================================
/* 自定义类型和字体颜色 */
#define MYDEBUG(TYPE, COLOR, FMT, ARGS...) \
        do \
        { \
            if(DEBUG_ENABLE && (DEBUG_##TYPE <= LOG_LEVEL)) \
            { \
                printf(COLOR); \
                printf("[%s][%s:%s:%d]"FMT, TYPE, __FILE__, __FUNCTION__, __LINE__, ##ARGS); \
                printf(CLS_ALL"\n"); \
            } \
        } while(0)
//===========================================================================
/* 平时调试使用 */
#include <string.h>
#include <time.h> // 时间函数用
#define filename(x) (strrchr(x,'/')?(strrchr(x,'/')+1):x)
#define NORMAL_DEBUG 1 // 注意使用下边两个宏需要自己添加换行，比较符合平时的习惯
// 获取时间函数
char *timeString(void)
{
  struct timespec ts;
  clock_gettime(CLOCK_REALTIME, &ts);
  struct tm *timeinfo = localtime(&ts.tv_sec);
  static char timeString[30];
  sprintf(timeString, "%.2d-%.2d %.2d:%.2d:%.2d", 
          timeinfo->tm_mon + 1, 
          timeinfo->tm_mday, 
          timeinfo->tm_hour, 
          timeinfo->tm_min, 
          timeinfo->tm_sec);
  return timeString;
}
// 获取系统时间戳 单位 ms
int sys_pts_get_ms(void)
{
    struct timespec tv;
    long long lasttime = 0;

    clock_gettime(CLOCK_MONOTONIC, &tv);
    lasttime = tv.tv_sec * 1000 + tv.tv_nsec / (1000 * 1000);
    return lasttime;

}

#define HKPRT(fmt...) \
        do \
        { \
            if(NORMAL_DEBUG) \
            { \
                printf(F_YELLOW); \
                printf("[%s][HK_LOG][%s:%d][%s]", timeString(), filename(__FILE__), __LINE__, __FUNCTION__); \
                printf(CLS_ALL); \
                printf(fmt); \
            } \
        } while(0)
#define HKPRTE(fmt...) \
        do \
        { \
            if(NORMAL_DEBUG) \
            { \
                printf(F_RED); \
                printf("[%s][HK_LOG][%s:%d][%s]", timeString(), filename(__FILE__), __LINE__, __FUNCTION__); \
                printf(CLS_ALL); \
                printf(fmt); \
            } \
        } while(0)

#endif