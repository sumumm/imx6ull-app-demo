#include <stdio.h>
#include "debug_printf.h"

int main(int argc, char *argv[])
{
    int *p = NULL;
    int a = 100;
    MYDEBUG(INFO, F_CYAN, "hello world!");
    HKPRT("hello world!\n");
    HKPRTE("hello world!\n");
    HKPRT("hello world! %s\n", "sumu");
    HKPRTE("hello world! %s\n", "sumu");
    return 0;
}
