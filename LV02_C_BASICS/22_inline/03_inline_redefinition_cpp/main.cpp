#include <iostream>
#include "test.h"

using namespace std;

inline int A::max()
{
    return a > b ? a : b;
}

int main()
{
    A a(3, 5);
    cout<<a.max()<<endl;
    return 0;
}