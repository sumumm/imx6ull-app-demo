#ifndef __COMMON_H__
#define __COMMON_H__

#include <stdio.h>

#ifndef NULL
#define NULL (0)
#endif

//#define TEST
/**
 * @brief  sstrrchr()
 * @note   sstrrchr() 是一个内联函数，在调用的时候会直接展开。定义成内联函数的话可以方便的放在.h文件中
 *         注意，是否会展开取决于编译的时候的优化选项
 * @param [in] s: The string to be searched
 * @param [in] c: The character to search for
 * @param [out]
 * @retval 
 */
#if TEST
inline char *sstrrchr(const char *s, int c)
#else
static inline char *sstrrchr(const char *s, int c)
#endif
{
	const char *last = NULL;
	do {
		if (*s == (char)c)
			last = s;
	} while (*s++);
	return (char *)last;
}
#define __FILE__NAME2(str) (sstrrchr(str,'/')?(sstrrchr(str,'/')+1):str)

#define PRT(fmt, args...)  \
        do                 \
        {                  \
            printf("[INFO][%s:%d][%s]"fmt, __FILE__NAME2(__FILE__), __LINE__, __FUNCTION__, ##args);\
        } while(0)

#endif /* __COMMON_H__ */
