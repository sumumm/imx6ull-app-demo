#include <stdio.h>

#define TABLE_MULTI1(x) (x*x)
// TABLE_MULTI(10+10) // 结果希望是400，但是宏的调用结果为(10+10*10+10) = 120

#define TABLE_MULTI2(x) ((x)*(x)) //加上括号？
// TABLE_MULTI(a++) //希望是(a+1)*(a+1)的结果，a=3的时候实际上是(a++)*(a++) = 12

int main(int argc, const char * argv[]) 
{
    int a = 3;

    printf("TABLE_MULTI1(10+10)=%d\n", TABLE_MULTI1(10+10));
    printf("TABLE_MULTI2(a++)=%d\n", TABLE_MULTI2(a++));

    return 0;
}