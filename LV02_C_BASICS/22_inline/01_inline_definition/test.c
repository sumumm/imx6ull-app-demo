#include <stdio.h>

inline void inline_function(void)
{
    printf("[inline_function]========= Get!!!\n");
}

int main(int argc, const char * argv[]) 
{
    inline_function();
    return 0;
}