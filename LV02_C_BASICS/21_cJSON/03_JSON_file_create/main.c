#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "../cJSON/cJSON.h"


// "interest": {
// 	"basketball": "篮球",
// 	"badminton": "羽毛球"
// }

/**
 * @brief  {}
 * @note   
 * @param [in]
 * @param [out]
 * @retval 
 */
int cJSON_create_demo1(void)
{
    char* str = NULL;
    // 定义对象 { }
    cJSON *interest = cJSON_CreateObject();
    // 插入元素，对应 键值对
    cJSON_AddItemToObject(interest, "basketball", cJSON_CreateString("篮球"));		// 当值是字符串时，需要使用函数cJSON_CreateString()创建
    cJSON_AddItemToObject(interest, "badminton", cJSON_CreateString("羽毛球"));
    // 或者使用宏进行添加
    //cJSON_AddStringToObject(interest, "badminton", "羽毛球");	// 或者这样写

    str = cJSON_Print(interest);
    printf("%s\n", str);

    cJSON_Delete(interest);

    return 0;
}

// "color": [ "black", "white"]

/**
 * @brief  []
 * @note   
 * @param [in]
 * @param [out]
 * @retval 
 */
int cJSON_create_demo2(void)
{
    char* str = NULL;
    // 定义 [ ] 数组
    cJSON *color = cJSON_CreateArray();
    // 往数组中添加元素
    cJSON_AddItemToArray(color, cJSON_CreateString("black"));
    cJSON_AddItemToArray(color, cJSON_CreateString("white"));

    str = cJSON_Print(color);
    printf("%s\n", str);

    cJSON_Delete(color);

    return 0;
}

// "subject": [
// 	{ "name": "语文", "score": 70 },
// 	{ "name": "数学", "score": 88.5 },
// ]

/**
 * @brief  [ { }, { } ]
 * @note   
 * @param [in]
 * @param [out]
 * @retval 
 */
int cJSON_create_demo3(void)
{
    char* str = NULL;

    // 定义 { } 对象
    cJSON *subjectObject1 = cJSON_CreateObject();
    cJSON_AddItemToObject(subjectObject1, "name", cJSON_CreateString("语文"));
    cJSON_AddItemToObject(subjectObject1, "score", cJSON_CreateNumber(70));		// 当值是数字时，需要使用函数cJSON_CreateNumber()创建
    //cJSON_AddNumberToObject(subjectObject1, "price", 66.6);	// 或者这样写

    cJSON *subjectObject2 = cJSON_CreateObject();
    cJSON_AddItemToObject(subjectObject2, "name", cJSON_CreateString("数学"));
    cJSON_AddItemToObject(subjectObject2, "score", cJSON_CreateNumber(88.5));

    // 定义 [ ] 数组
    cJSON *subject = cJSON_CreateArray();
    // 往数组中添加元素
    cJSON_AddItemToArray(subject, subjectObject1);
    cJSON_AddItemToArray(subject, subjectObject2);

    str = cJSON_Print(subject);
    printf("%s\n", str);

    cJSON_Delete(subject);

    return 0;
}

// "education": [
//     [ "小学", "初中" ],
//     [ "高中", "大学" ]
// ]

/**
 * @brief  [ [ ], [ ] ]
 * @note   
 * @param [in]
 * @param [out]
 * @retval 
 */
int cJSON_create_demo4(void)
{
    char* str = NULL;

    // 定义 [ ] 数组
    cJSON *education1 = cJSON_CreateArray();
    cJSON_AddItemToArray(education1, cJSON_CreateString("小学"));
    cJSON_AddItemToArray(education1, cJSON_CreateString("初中"));

    cJSON *education2 = cJSON_CreateArray();
    cJSON_AddItemToArray(education2, cJSON_CreateString("高中"));
    cJSON_AddItemToArray(education2, cJSON_CreateString("大学"));

    // 定义 [ ] 数组
    cJSON *education = cJSON_CreateArray();
    cJSON_AddItemToArray(education, education1);
    cJSON_AddItemToArray(education, education2);

    str = cJSON_Print(education);
    printf("%s\n", str);

    cJSON_Delete(education);

    return 0;
}

// "languages": {
// 	"serialOne": { "language": "汉语", "grade": 10 },
// 	"serialTwo": { "language": "英语", "grade": 6}
// }

/**
 * @brief  { { }, { } }
 * @note   
 * @param [in]
 * @param [out]
 * @retval 
 */
int cJSON_create_demo5(void)
{
    char* str = NULL;

    // 定义对象 { }
    cJSON *serialOne = cJSON_CreateObject();
    cJSON_AddItemToObject(serialOne, "language", cJSON_CreateString("汉语"));		
    cJSON_AddItemToObject(serialOne, "grade", cJSON_CreateNumber(10));

    cJSON *serialTwo = cJSON_CreateObject();
    cJSON_AddItemToObject(serialTwo, "language", cJSON_CreateString("英语"));
    cJSON_AddItemToObject(serialTwo, "grade", cJSON_CreateNumber(6));

    // 定义对象 { }
    cJSON *languages = cJSON_CreateObject();
    cJSON_AddItemToObject(languages, "serialOne", serialOne);
    cJSON_AddItemToObject(languages, "serialTwo", serialTwo);

    str = cJSON_Print(languages);
    printf("%s\n", str);

    cJSON_Delete(languages);

    return 0;
}

int cJSON_file_create_demo()
{
    cJSON* root = NULL;
    cJSON* cjson_address = NULL;
    cJSON* cjson_skill = NULL;
    char* str = NULL;

    /* 创建一个JSON数据对象(链表头结点) */
    root = cJSON_CreateObject();

    /* 添加一条字符串类型的JSON数据(添加一个链表节点) */
    cJSON_AddStringToObject(root, "name", "sumu");

    /* 添加一条整数类型的JSON数据(添加一个链表节点) */
    cJSON_AddNumberToObject(root, "age", 28);

    /* 添加一条浮点类型的JSON数据(添加一个链表节点) */
    cJSON_AddNumberToObject(root, "weight", 69.8);

    /* 添加一个嵌套的JSON数据（添加一个链表节点） */
    cjson_address = cJSON_CreateObject();
    cJSON_AddStringToObject(cjson_address, "country", "China");
    cJSON_AddStringToObject(cjson_address, "city", "shanghai");
    cJSON_AddNumberToObject(cjson_address, "zip-code", 999999);
    cJSON_AddItemToObject(root, "address", cjson_address);

    /* 添加一个数组类型的JSON数据(添加一个链表节点) */
    cjson_skill = cJSON_CreateArray();
    cJSON_AddItemToArray(cjson_skill, cJSON_CreateString( "C" ));
    cJSON_AddItemToArray(cjson_skill, cJSON_CreateString( "html" ));
    cJSON_AddItemToArray(cjson_skill, cJSON_CreateString( "Python" ));
    cJSON_AddItemToObject(root, "skill", cjson_skill);

    /* 添加一个值为 False 的布尔类型的JSON数据(添加一个链表节点) */
    cJSON_AddFalseToObject(root, "student");

    /* 打印JSON对象(整条链表)的所有数据 */
    str = cJSON_Print(root);
    printf("%s\n", str);

    // 打开文件
    FILE *pFile = NULL;
    pFile = fopen("./cfg.json", "w");	// FILE_NAME ==> "cfg.json"
    if (pFile == NULL) {
        printf("Open file fail!\n");

        // 释放指针内存
        cJSON_Delete(root);
        return -1;
    }
    int ret = fwrite(str, sizeof(char), strlen(str), pFile);
    if (ret == EOF) 
    {
        printf("写入文件失败！\n");
    }
    cJSON_Delete(root);
    free(str);
    return 0;
}
int main(int argc, const char * argv[])
{
    // cJSON_create_demo1(); // { }
    // cJSON_create_demo2(); // [ ]
    // cJSON_create_demo3(); // [ { }, { } ]
    // cJSON_create_demo4(); // [ [ ], [ ] ]
    // cJSON_create_demo5(); // { { }, { } }
    cJSON_file_create_demo();//综合demo
    return 0;
}