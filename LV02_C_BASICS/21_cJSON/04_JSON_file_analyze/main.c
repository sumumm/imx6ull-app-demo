#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include "../cJSON/cJSON.h"

#define FILE_NAME "cfg.json"

// "interest": {
// 	"basketball": "篮球",
// 	"badminton": "羽毛球"
// }

/**
 * @brief  {}
 * @note   
 * @param [in]
 * @param [out]
 * @retval 
 */
int cJSON_create_demo1(void)
{
    char* str = NULL;
    // 定义对象 { }
    cJSON *interest = cJSON_CreateObject();
    // 插入元素，对应 键值对
    cJSON_AddItemToObject(interest, "basketball", cJSON_CreateString("篮球"));		// 当值是字符串时，需要使用函数cJSON_CreateString()创建
    cJSON_AddItemToObject(interest, "badminton", cJSON_CreateString("羽毛球"));
    // 或者使用宏进行添加
    //cJSON_AddStringToObject(interest, "badminton", "羽毛球");	// 或者这样写

    str = cJSON_Print(interest);
    printf("%s\n", str);

    cJSON_Delete(interest);

    return 0;
}

// "color": [ "black", "white"]

/**
 * @brief  []
 * @note   
 * @param [in]
 * @param [out]
 * @retval 
 */
int cJSON_create_demo2(void)
{
    char* str = NULL;
    // 定义 [ ] 数组
    cJSON *color = cJSON_CreateArray();
    // 往数组中添加元素
    cJSON_AddItemToArray(color, cJSON_CreateString("black"));
    cJSON_AddItemToArray(color, cJSON_CreateString("white"));

    str = cJSON_Print(color);
    printf("%s\n", str);

    cJSON_Delete(color);

    return 0;
}

// "subject": [
// 	{ "name": "语文", "score": 70 },
// 	{ "name": "数学", "score": 88.5 },
// ]

/**
 * @brief  [ { }, { } ]
 * @note   
 * @param [in]
 * @param [out]
 * @retval 
 */
int cJSON_create_demo3(void)
{
    char* str = NULL;

    // 定义 { } 对象
    cJSON *subjectObject1 = cJSON_CreateObject();
    cJSON_AddItemToObject(subjectObject1, "name", cJSON_CreateString("语文"));
    cJSON_AddItemToObject(subjectObject1, "score", cJSON_CreateNumber(70));		// 当值是数字时，需要使用函数cJSON_CreateNumber()创建
    //cJSON_AddNumberToObject(subjectObject1, "price", 66.6);	// 或者这样写

    cJSON *subjectObject2 = cJSON_CreateObject();
    cJSON_AddItemToObject(subjectObject2, "name", cJSON_CreateString("数学"));
    cJSON_AddItemToObject(subjectObject2, "score", cJSON_CreateNumber(88.5));

    // 定义 [ ] 数组
    cJSON *subject = cJSON_CreateArray();
    // 往数组中添加元素
    cJSON_AddItemToArray(subject, subjectObject1);
    cJSON_AddItemToArray(subject, subjectObject2);

    str = cJSON_Print(subject);
    printf("%s\n", str);

    cJSON_Delete(subject);

    return 0;
}

// "education": [
//     [ "小学", "初中" ],
//     [ "高中", "大学" ]
// ]

/**
 * @brief  [ [ ], [ ] ]
 * @note   
 * @param [in]
 * @param [out]
 * @retval 
 */
int cJSON_create_demo4(void)
{
    char* str = NULL;

    // 定义 [ ] 数组
    cJSON *education1 = cJSON_CreateArray();
    cJSON_AddItemToArray(education1, cJSON_CreateString("小学"));
    cJSON_AddItemToArray(education1, cJSON_CreateString("初中"));

    cJSON *education2 = cJSON_CreateArray();
    cJSON_AddItemToArray(education2, cJSON_CreateString("高中"));
    cJSON_AddItemToArray(education2, cJSON_CreateString("大学"));

    // 定义 [ ] 数组
    cJSON *education = cJSON_CreateArray();
    cJSON_AddItemToArray(education, education1);
    cJSON_AddItemToArray(education, education2);

    str = cJSON_Print(education);
    printf("%s\n", str);

    cJSON_Delete(education);

    return 0;
}

// "languages": {
// 	"serialOne": { "language": "汉语", "grade": 10 },
// 	"serialTwo": { "language": "英语", "grade": 6}
// }

/**
 * @brief  { { }, { } }
 * @note   
 * @param [in]
 * @param [out]
 * @retval 
 */
int cJSON_create_demo5(void)
{
    char* str = NULL;

    // 定义对象 { }
    cJSON *serialOne = cJSON_CreateObject();
    cJSON_AddItemToObject(serialOne, "language", cJSON_CreateString("汉语"));		
    cJSON_AddItemToObject(serialOne, "grade", cJSON_CreateNumber(10));

    cJSON *serialTwo = cJSON_CreateObject();
    cJSON_AddItemToObject(serialTwo, "language", cJSON_CreateString("英语"));
    cJSON_AddItemToObject(serialTwo, "grade", cJSON_CreateNumber(6));

    // 定义对象 { }
    cJSON *languages = cJSON_CreateObject();
    cJSON_AddItemToObject(languages, "serialOne", serialOne);
    cJSON_AddItemToObject(languages, "serialTwo", serialTwo);

    str = cJSON_Print(languages);
    printf("%s\n", str);

    cJSON_Delete(languages);

    return 0;
}

int cJSON_file_create_demo()
{
    cJSON* root = NULL;
    cJSON* cjson_address = NULL;
    cJSON* cjson_skill = NULL;
    char* str = NULL;

    /* 创建一个JSON数据对象(链表头结点) */
    root = cJSON_CreateObject();

    /* 添加一条字符串类型的JSON数据(添加一个链表节点) */
    cJSON_AddStringToObject(root, "name", "sumu");

    /* 添加一条整数类型的JSON数据(添加一个链表节点) */
    cJSON_AddNumberToObject(root, "age", 28);

    /* 添加一条浮点类型的JSON数据(添加一个链表节点) */
    cJSON_AddNumberToObject(root, "weight", 69.8);

    /* 添加一个嵌套的JSON数据（添加一个链表节点） */
    cjson_address = cJSON_CreateObject();
    cJSON_AddStringToObject(cjson_address, "country", "China");
    cJSON_AddStringToObject(cjson_address, "city", "shanghai");
    cJSON_AddNumberToObject(cjson_address, "zip-code", 999999);
    cJSON_AddItemToObject(root, "address", cjson_address);

    /* 添加一个数组类型的JSON数据(添加一个链表节点) */
    cjson_skill = cJSON_CreateArray();
    cJSON_AddItemToArray(cjson_skill, cJSON_CreateString( "C" ));
    cJSON_AddItemToArray(cjson_skill, cJSON_CreateString( "html" ));
    cJSON_AddItemToArray(cjson_skill, cJSON_CreateString( "Python" ));
    cJSON_AddItemToObject(root, "skill", cjson_skill);

    /* 添加一个值为 False 的布尔类型的JSON数据(添加一个链表节点) */
    cJSON_AddFalseToObject(root, "student");

    /* 打印JSON对象(整条链表)的所有数据 */
    str = cJSON_Print(root);
    printf("%s\n", str);

    // 打开文件
    FILE *pFile = NULL;
    pFile = fopen(FILE_NAME, "w");	// FILE_NAME ==> "cfg.json"
    if (pFile == NULL) {
        printf("Open file fail!FILE_NAME=%s\n", FILE_NAME);

        // 释放指针内存
        cJSON_Delete(root);
        return -1;
    }
    int ret = fwrite(str, sizeof(char), strlen(str), pFile);
    if (ret == EOF) 
    {
        printf("写入文件失败！\n");
    }
    cJSON_Delete(root);
    free(str);
    return 0;
}

long get_file_size(FILE *stream)
{
	long file_size = -1;
	long cur_offset = ftell(stream);	// 获取当前偏移位置
	if (cur_offset == -1) {
		printf("ftell failed :%s\n", strerror(errno));
		return -1;
	}
	if (fseek(stream, 0, SEEK_END) != 0) {	// 移动文件指针到文件末尾
		printf("fseek failed: %s\n", strerror(errno));
		return -1;
	}
	file_size = ftell(stream);	// 获取此时偏移值，即文件大小
	if (file_size == -1) {
		printf("ftell failed :%s\n", strerror(errno));
	}
	if (fseek(stream, cur_offset, SEEK_SET) != 0) {	// 将文件指针恢复初始位置
		printf("fseek failed: %s\n", strerror(errno));
		return -1;
	}
	return file_size;
}

int cJSON_file_analyze_demo(void)
{
    cJSON* root = NULL;

    cJSON* cjson_name = NULL;
    cJSON* cjson_age = NULL;
    cJSON* cjson_weight = NULL;

    cJSON* cjson_address = NULL;
    cJSON* cjson_address_country = NULL;
    cJSON* cjson_address_city = NULL;
    cJSON* cjson_address_zipcode = NULL;

    cJSON* cjson_skill = NULL;
    cJSON* cjson_skill_item = NULL;
    int    skill_array_size = 0;
    int    i = 0;

    cJSON* cjson_student = NULL;

    // 读取json文件
    FILE *pFile = NULL;
    int file_size = 0;
    pFile = fopen(FILE_NAME, "r");
    if (pFile == NULL) 
    {
        printf("Open file fail！\n");
        return -1;
    }
    file_size = (int)get_file_size(pFile);

    // 分配符合文件大小的内存
    char *JSON_data = (char *)malloc(sizeof(char) * file_size + 1);
    memset(JSON_data, 0, file_size + 1);
    // 读取文件中的json字符串
    int read_size = fread(JSON_data, sizeof(char), file_size, pFile);
    if (read_size < file_size) 
    {
        printf("读取文件失败！\n");
        fclose(pFile);
        return -1;
    }

    /* 解析整段JSO数据 */
    root = cJSON_Parse(JSON_data);
    if(root == NULL)
    {
        printf("parse fail.\n");
        return -1;
    }

    /* 依次根据名称提取JSON数据（键值对） */
    cjson_name = cJSON_GetObjectItem(root, "name");
    cjson_age = cJSON_GetObjectItem(root, "age");
    cjson_weight = cJSON_GetObjectItem(root, "weight");

    printf("name: %s\n", cjson_name->valuestring);
    printf("age:%d\n", cjson_age->valueint);
    printf("weight:%.1f\n", cjson_weight->valuedouble);

    /* 解析嵌套json数据 */
    cjson_address = cJSON_GetObjectItem(root, "address");
    cjson_address_country = cJSON_GetObjectItem(cjson_address, "country");
    cjson_address_city = cJSON_GetObjectItem(cjson_address, "city");
    cjson_address_zipcode = cJSON_GetObjectItem(cjson_address, "zip-code");
    printf("address-country:%s\naddress-city:%s\naddress-zipcode:%d\n", 
        cjson_address_country->valuestring, cjson_address_city->valuestring, cjson_address_zipcode->valueint);

    /* 解析数组 */
    cjson_skill = cJSON_GetObjectItem(root, "skill");
    skill_array_size = cJSON_GetArraySize(cjson_skill);
    printf("skill:[");
    for(i = 0; i < skill_array_size; i++)
    {
        cjson_skill_item = cJSON_GetArrayItem(cjson_skill, i);
        printf("%s,", cjson_skill_item->valuestring);
    }
    printf("\b]\n");

    /* 解析布尔型数据 */
    cjson_student = cJSON_GetObjectItem(root, "student");
    if(cjson_student->valueint == 0)
    {
        printf("student: false\n");
    }
    else
    {
        printf("student:error\n");
    }

    return 0;
}


int main(int argc, const char * argv[])
{
    // cJSON_create_demo1(); // { }
    // cJSON_create_demo2(); // [ ]
    // cJSON_create_demo3(); // [ { }, { } ]
    // cJSON_create_demo4(); // [ [ ], [ ] ]
    // cJSON_create_demo5(); // { { }, { } }
    cJSON_file_analyze_demo();//综合demo
    return 0;
}