注意：windows下没有软链接的概念，所以当这个目录位于共享文件夹中的时候它也存在于windows下，所以软链接时拷贝不成功的，这里需要手动复制源文件然后重命名。

```shell
lrwxrwxrwx 1 sumu sumu   14 9月  22 14:43 libts.so -> libts.so.0.9.1
lrwxrwxrwx 1 sumu sumu   14 9月  22 14:43 libts.so.0 -> libts.so.0.9.1
-rwxr-xr-x 1 sumu sumu  17K 9月  22 14:43 libts.so.0.9.1
```