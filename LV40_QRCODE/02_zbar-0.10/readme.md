这个0.10版本的zbar有个坑，至少在写这个demo的时候（2025-01-14）还存在。这个demo是用于缺陷的复现。

问题就是，进入 [草料二维码生成器](https://cli.im/) ，默认参数下的二维码无法识别，需要修改一个样式后才能识别。是不是很离谱？？？？不是很清楚草料二维码的生成逻辑到底有什么区别，我尝试过修改容错率，图片分辨率等相关信息，在默认的样式下都没法识别出来，换了样式之后，就可以了，最后没有去深究了。没有办法修改人家的逻辑，那就更新下zbar的版本吧，换了zbar-0.23版本就可以正常识别了。但是暂时没有深究到底是哪里的修改可以修复这个问题。

<img src="readme/img/image-20250114224550188.png" alt="image-20250114224550188" />

> （1）并不是所有的版本下载下来之后都直接有configure文件，有些版本是没有的，需要自己生成，但是自己生成又总是有一堆的问题，不如选直接带有configure的版本。
>
> （2）关于0.10版本，在网上可能会在这几个地方找到0.10版本的源码：
>
> ```html
> linuxtv：https://linuxtv.org/downloads/zbar/zbar-0.10.tar.bz2
> sourceforge：https://sourceforge.net/projects/zbar/files/zbar/0.10/zbar-0.10.tar.bz2/download
> GitHub：https://github.com/ZBar/ZBar/releases/tag/0.10
> ```
>
> 只有 sourceforge 中下载的带有configure文件，其他两个都没有，我自己折腾生成configure文件的时候不是这里报错就是那里报错的，就感觉很麻烦，不如选择带有 configure 文件的，如果一定要用0.10版本的话，可以用以下命令下载：
>
> ```shell
> wget https://jaist.dl.sourceforge.net/project/zbar/zbar/0.10/zbar-0.10.tar.bz2
> ```