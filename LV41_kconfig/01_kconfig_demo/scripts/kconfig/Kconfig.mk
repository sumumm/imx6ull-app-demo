TARGET_mconf    ?= mconf
TARGET_conf     ?= conf

CROSS_COMPILE   ?=
CC 				:= $(CROSS_COMPILE)gcc
LD				:= $(CROSS_COMPILE)ld
OBJCOPY 		:= $(CROSS_COMPILE)objcopy
OBJDUMP 		:= $(CROSS_COMPILE)objdump

HOSTCFLAGS       = -Wall -Wstrict-prototypes -O2 -fomit-frame-pointer  -std=gnu11
COBJ_FLAGS      := $(HOSTCFLAGS)

OBJ_DIR         ?= ./
SRC_DIR         ?= ./

ifdef KBUILD_KCONFIG
Kconfig := $(KBUILD_KCONFIG)
else
Kconfig := Kconfig
endif

# SHELL used by kbuild
CONFIG_SHELL    := $(shell if [ -x "$$BASH" ]; then echo $$BASH; \
					else if [ -x /bin/bash ]; then echo /bin/bash; \
					else echo sh; fi ; fi)

# lxdialog stuff
check-lxdialog  := ./lxdialog/check-lxdialog.sh

# Use recursively expanded variables so we do not call gcc unless
# we really need to do so. (Do not call gcc as part of make mrproper)
HOST_EXTRACFLAGS        += $(shell $(CONFIG_SHELL) $(check-lxdialog) -ccflags) \
                    		-DLOCALE
# Add environment specific flags
HOST_EXTRACFLAGS        += $(shell $(CONFIG_SHELL) ./check.sh $(CC) $(HOSTCFLAGS))

# generated files seem to need this to find local include files
HOSTCFLAGS_zconf.lex.o	:= -I$(SRC_DIR)
HOSTCFLAGS_zconf.tab.o	:= -I$(SRC_DIR)
HOSTLOADLIBES_mconf      = $(shell $(CONFIG_SHELL) $(check-lxdialog) -ldflags $(CC))
HOSTLOADLIBES_conf       = 

COBJ_FLAGS      += $(HOST_EXTRACFLAGS)

lxdialog        := lxdialog/checklist.o lxdialog/util.o lxdialog/inputbox.o
lxdialog        += lxdialog/textbox.o lxdialog/yesno.o lxdialog/menubox.o

conf-objs	    := conf.o  zconf.tab.o
mconf-objs      := mconf.o zconf.tab.o $(lxdialog)

$(TARGET_mconf): $(mconf-objs)
	$(CC) -o $@ $(mconf-objs) $(HOSTLOADLIBES_mconf)

$(TARGET_conf): $(conf-objs)
	$(CC) -o $@ $(conf-objs) $(HOSTLOADLIBES_conf)


zconf.tab.o: zconf.tab.c zconf.lex.c
	$(CC) -Wp,-MD,.$@.d $(COBJ_FLAGS) $(HOSTCFLAGS_zconf.tab.o) -c -o $@ zconf.tab.c

zconf.tab.c:
	bison -o $@ -t -l zconf.y

zconf.lex.c:
	flex -o $@ -L zconf.l

$(lxdialog): lxdialog/%.o : lxdialog/%.c
	$(CC) -Wp,-MD,$@.d $(COBJ_FLAGS) -c $< -o $@

$(TARGET_mconf).o:
	$(CC) -Wp,-MD,.$@.d $(COBJ_FLAGS) -Iscripts/kconfig -c -o $@ mconf.c

$(TARGET_conf).o:
	$(CC) -Wp,-MD,.$@.d $(COBJ_FLAGS) -Iscripts/kconfig -c -o $@ conf.c

menuconfig: $(TARGET_mconf)
	./$(TARGET_mconf) Kconfig
	@python config_head_create.py .config config.h 0
	@rm -f .config.old .config.cmd

%_defconfig: $(TARGET_conf)
	@if [ -e ./configs/$@ ];then \
		./$(TARGET_conf)  --defconfig=lxdialog/../configs/$@ Kconfig; \
	else \
		echo "$@ not exists"; \
	fi
	@python config_head_create.py .config config.h 0
	@rm -f .config.old .config.cmd

.PHONY: clean clean_c clean_obj clean_d clean_config

clean: clean_c clean_obj clean_d clean_config

clean_c:
	@rm -rvf zconf.lex.c
	@rm -rvf zconf.tab.c

clean_obj:
	@find . -name $(TARGET_mconf) -exec rm -rvf {} \;
	@find . -name $(TARGET_conf) -exec rm -rvf {} \;
	@find . -name \*.o -exec rm -rvf {} \;

clean_d:
	@find . -name \*.d -exec rm -rvf {} \;

clean_config:
	@rm -rvf .config .config.old config.h
