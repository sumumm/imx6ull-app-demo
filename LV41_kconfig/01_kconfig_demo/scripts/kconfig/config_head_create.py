#!/usr/bin/env python3

import os
import sys

def write_config(config_filename, filename, chipname):
    config_h = open(filename, 'w')
    config_h.write('#ifndef __CONFIG_H__\n')
    config_h.write('#define __CONFIG_H__\n\n')

    config = open(config_filename, 'r')

    empty_line = 1

    for line in config:
        line = line.lstrip(' ').replace('\n', '').replace('\r', '')

        if len(line) == 0:
            continue

        if line[0] == '#':
            if len(line) == 1:
                if empty_line:
                    continue

                config_h.write('\n')
                empty_line = 1
                continue

            line = line[2:]

            config_h.write('/*%s */\n' % line)
            empty_line = 0

        else:
            empty_line = 0
            setting = line.split('=')
            if len(setting) >= 2:
                if setting[1] == 'y':
                    config_h.write('#define %s\n' % setting[0])
                else:
                    config_h.write('#define %s %s\n' % (setting[0], setting[1]))

    config_h.write('\n')
    config_h.write('#endif\n')
    config_h.close()

if __name__ == '__main__':

    if len(sys.argv) < 2:
        sys.exit(1)

    config_filename = sys.argv[1]
    filename = sys.argv[2]
    chipname = sys.argv[3]

    write_config(config_filename, filename, chipname)
