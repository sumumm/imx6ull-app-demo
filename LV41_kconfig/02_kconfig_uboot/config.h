#ifndef __CONFIG_H__
#define __CONFIG_H__

/*Automatically generated file; DO NOT EDIT. */
/*mconf compile test */

/*menu1 */

#define CONFIG_MY_TESTCONFIG1-1
/*CONFIG_MY_TESTCONFIG1-2 is not set */

/*menu2 */

/*CONFIG_MY_TESTCONFIG2-1 is not set */

/*menu3 */

/*CONFIG_MY_TESTCONFIG3-1 is not set */

/*My test menu one */

#define CONFIG_MY_TESTCONFIG1
#define CONFIG_MY_TESTCONFIG2
#define CONFIG_MY_TESTCONFIG3 "sumu"

/*My test menu two */

/*My test menu two test 1 */

#define CONFIG_MY_MENU_THREE_TEST1
/*CONFIG_MY_MENU_THREE_TEST2 is not set */
#define CONFIG_MY_MENU_CHOICE_TEST1
#define CONFIG_ARM
/*CONFIG_X86 is not set */

#endif
